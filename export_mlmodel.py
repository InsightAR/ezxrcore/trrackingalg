# ============================================================
#   Copyright (c) 2022, EZXR Inc. All rights reserved
#   File        : export_mlmodel.py
#   Author      : qinyu@ezxr.com
#   Created date: 2023/6/15 18:48
#   Description : 
# ============================================================
from loguru import logger

import torch
from pathlib import Path
from torch import nn

from yolox.exp import get_exp
from yolox.models.network_blocks import SiLU
from yolox.utils import replace_module
import argparse
import os
import coremltools as ct


def make_parser():
    parser = argparse.ArgumentParser("YOLOX onnx deploy")
    parser.add_argument(
        "--output_name", type=str, default="detector.mlmodel", help="output name of models"
    )
    parser.add_argument(
        "--input", default="images", type=str, help="input node name of onnx model"
    )
    parser.add_argument(
        "--output", default="output", type=str, help="output node name of onnx model"
    )
    parser.add_argument(
        "-o", "--opset", default=11, type=int, help="onnx opset version"
    )
    parser.add_argument("--no-onnxsim", action="store_true", help="use onnxsim or not")
    parser.add_argument(
        "-f",
        "--exp_file",
        default=None,
        type=str,
        help="expriment description file",
    )
    parser.add_argument("-expn", "--experiment-name", type=str, default=None)
    parser.add_argument("-n", "--name", type=str, default=None, help="model name")
    parser.add_argument("-c", "--ckpt", default=None, type=str, help="ckpt path")
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    return parser


@logger.catch
def main_for_nanodet():
    args = make_parser().parse_args()
    logger.info("args value: {}".format(args))
    exp = get_exp(args.exp_file, args.name)
    exp.merge(args.opts)

    if not args.experiment_name:
        args.experiment_name = exp.exp_name

    model = exp.get_model()
    if args.ckpt is None:
        file_name = os.path.join(exp.output_dir, args.experiment_name)
        ckpt_file = os.path.join(file_name, "best_ckpt.pth.tar")
    else:
        ckpt_file = args.ckpt

    # load the model state dict
    ckpt = torch.load(ckpt_file, map_location="cpu")

    model.eval()
    if "model" in ckpt:
        ckpt = ckpt["model"]
    model.load_state_dict(ckpt)
    model = replace_module(model, nn.SiLU, SiLU)
    model.head.decode_in_inference = False
    model.eval()

    logger.info("loading checkpoint done.")
    dummy_input = torch.randn(1, 3, exp.test_size[0], exp.test_size[1])
    model_traced = torch.jit.trace(model, example_inputs=dummy_input)

    model_traced.save("yolox.torchscript.pt")
    print("Finished export to TorchScript")

    mlmodel = ct.convert("yolox.torchscript.pt",
                         source="pytorch",
                         # 103.53, 116.28, 123.675
                         inputs=[ct.ImageType(name="images",
                                              bias=[-1.0 * 103.53 * 0.017, -1.0 * 116.779 * 0.017,
                                                    -1.0 * 123.68 * 0.017],
                                              scale=0.017, shape=(1, 3, exp.test_size[0], exp.test_size[1]),
                                              color_layout=ct.colorlayout.RGB,
                                              channel_first=True)
                         ],
    )

    mlmodel.save('bytetrack_yolox_nano.mlmodel')



@logger.catch
def main_for_bytetrack_nano():
    args = make_parser().parse_args()
    logger.info("args value: {}".format(args))
    exp = get_exp(args.exp_file, args.name)
    exp.merge(args.opts)

    if not args.experiment_name:
        args.experiment_name = exp.exp_name

    model = exp.get_model()
    if args.ckpt is None:
        file_name = os.path.join(exp.output_dir, args.experiment_name)
        ckpt_file = os.path.join(file_name, "best_ckpt.pth.tar")
    else:
        ckpt_file = args.ckpt

    # load the model state dict
    ckpt = torch.load(ckpt_file, map_location="cpu")

    model.eval()
    if "model" in ckpt:
        ckpt = ckpt["model"]
    model.load_state_dict(ckpt)
    model = replace_module(model, nn.SiLU, SiLU)
    model.head.decode_in_inference = False

    logger.info("loading checkpoint done.")
    dummy_input = torch.randn(1, 3, exp.test_size[0], exp.test_size[1])
    model_traced = torch.jit.trace(model, example_inputs=dummy_input)

    model_saved_torchscript = str(Path(args.output_name).with_suffix(".torchscript.pt"))
    model_traced.save(model_saved_torchscript)
    print("Finished export to TorchScript")

    mlmodel = ct.convert(model_saved_torchscript,
                         source="pytorch",
                         # 103.53, 116.28, 123.675
                         inputs=[ct.ImageType(name="images",
                                              bias=[-1.0 * 123.675 * 0.017, -1.0 * 116.779 * 0.017,
                                                    -1.0 * 103.53 * 0.017],
                                              scale=0.017, shape=(1, 3, exp.test_size[0], exp.test_size[1]),
                                              color_layout=ct.colorlayout.RGB,
                                              channel_first=True)
                                 ],
                         )
    mlmodel.save(args.output_name)




if __name__ == '__main__':
    main_for_bytetrack_nano()